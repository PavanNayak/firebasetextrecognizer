package com.abvious.firebasetextrecognizer.Activities

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.MediaStore
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import com.abvious.firebasetextrecognizer.R
import com.abvious.firebasetextrecognizer.Utils.MyHelper

import java.io.File

open class BaseActivity : AppCompatActivity() {
    lateinit var imageFile: File

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val actionBar = supportActionBar
        if (actionBar != null) {
            //            actionBar.setDisplayHomeAsUpEnabled(true);
            //            actionBar.setTitle(getIntent().getStringExtra(ACTION_BAR_TITLE));
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_new, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_gallery -> checkStoragePermission(RC_STORAGE_PERMS1)
            R.id.action_camera -> checkStoragePermission(RC_STORAGE_PERMS2)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            RC_STORAGE_PERMS1 -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                selectPicture()
            } else {
                MyHelper.needPermission(this, requestCode, R.string.confirm_storage)
            }
            RC_STORAGE_PERMS2 -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openCamera()
            } else {
                MyHelper.needPermission(this, requestCode, R.string.confirm_camera)
            }
        }
    }

    fun checkStoragePermission(requestCode: Int) {
        when (requestCode) {
            RC_STORAGE_PERMS1 -> {
                val hasWriteExternalStoragePermission = ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                if (hasWriteExternalStoragePermission == PackageManager.PERMISSION_GRANTED) {
                    selectPicture()
                } else {
                    ActivityCompat.requestPermissions(
                        this,
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        requestCode
                    )
                }
            }
            RC_STORAGE_PERMS2 -> {
                val PERMISSIONS = arrayOf(
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    android.Manifest.permission.CAMERA
                )
                if (!hasPermissions(this, *PERMISSIONS)) {
                    ActivityCompat.requestPermissions(this, PERMISSIONS, requestCode)
                } else {
                    openCamera()
                }
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    private fun selectPicture() {
        imageFile = MyHelper.createTempFile(imageFile)
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, RC_SELECT_PICTURE)
    }

    private fun openCamera() {
        imageFile = MyHelper.createTempFile(imageFile)
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val photo = FileProvider.getUriForFile(this, "$packageName.provider", imageFile)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, photo)
        startActivityForResult(intent, RC_TAKE_PICTURE)
    }

    companion object {
        val RC_STORAGE_PERMS1 = 101
        val RC_STORAGE_PERMS2 = 102
        val RC_SELECT_PICTURE = 103
        val RC_TAKE_PICTURE = 104
        val ACTION_BAR_TITLE = "action_bar_title"

        fun hasPermissions(context: Context?, vararg permissions: String): Boolean {
            if (context != null && permissions != null) {
                for (permission in permissions) {
                    if (ActivityCompat.checkSelfPermission(
                            context,
                            permission
                        ) != PackageManager.PERMISSION_GRANTED
                    ) {
                        return false
                    }
                }
            }
            return true
        }
    }
}