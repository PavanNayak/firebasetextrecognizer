package com.abvious.firebasetextrecognizer.Activities

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView

import com.abvious.firebasetextrecognizer.R
import com.abvious.firebasetextrecognizer.Utils.MyHelper
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.cloud.FirebaseVisionCloudDetectorOptions
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.text.FirebaseVisionCloudTextRecognizerOptions
import com.google.firebase.ml.vision.text.FirebaseVisionText


import java.util.Arrays

class MainActivity : BaseActivity(), View.OnClickListener {

    private var mBitmap: Bitmap? = null
    private var mImageView: ImageView? = null
    private var mTextView: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        mTextView = findViewById(R.id.text_view)
        mImageView = findViewById(R.id.image_view)
        findViewById<View>(R.id.btn_device).setOnClickListener(this)
        findViewById<View>(R.id.btn_cloud).setOnClickListener(this)

    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_device -> if (mBitmap != null) {
                runTextRecognition()
            }
            R.id.btn_cloud -> if (mBitmap != null) {
                runCloudTextRecognition()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                BaseActivity.Companion.RC_STORAGE_PERMS1, BaseActivity.Companion.RC_STORAGE_PERMS2 -> checkStoragePermission(
                    requestCode
                )
                BaseActivity.Companion.RC_SELECT_PICTURE -> {
                    val dataUri = data!!.data
                    val path = MyHelper.getPath(this, dataUri)
                    if (path == null) {
                        mBitmap = MyHelper.resizeImage(imageFile, this, dataUri, mImageView!!)
                    } else {
                        mBitmap = MyHelper.resizeImage(imageFile, path, mImageView!!)
                    }
                    if (mBitmap != null) {
                        mTextView!!.text = null
                        mImageView!!.setImageBitmap(mBitmap)
                    }
                }
                BaseActivity.Companion.RC_TAKE_PICTURE -> {
                    mBitmap = MyHelper.resizeImage(imageFile, imageFile.path, mImageView!!)
                    if (mBitmap != null) {
                        mTextView!!.text = null
                        mImageView!!.setImageBitmap(mBitmap)
                    }
                }
            }
        }
    }

    private fun runTextRecognition() {
        val image = FirebaseVisionImage.fromBitmap(mBitmap!!)
        val detector = FirebaseVision.getInstance().onDeviceTextRecognizer
        detector.processImage(image)
            .addOnSuccessListener { texts -> processTextRecognitionResult(texts) }
            .addOnFailureListener { e -> e.printStackTrace() }
    }

    private fun runCloudTextRecognition() {
        MyHelper.showDialog(this)

        val options = FirebaseVisionCloudTextRecognizerOptions.Builder()
            .setLanguageHints(Arrays.asList("en", "hi"))
            .setModelType(FirebaseVisionCloudDetectorOptions.LATEST_MODEL)
            .build()

        val image = FirebaseVisionImage.fromBitmap(mBitmap!!)
        val detector = FirebaseVision.getInstance().getCloudTextRecognizer(options)

        detector.processImage(image).addOnSuccessListener { texts ->
            MyHelper.dismissDialog()
            processTextRecognitionResult(texts)
        }.addOnFailureListener { e ->
            MyHelper.dismissDialog()
            e.printStackTrace()
        }
    }

    private fun processTextRecognitionResult(firebaseVisionText: FirebaseVisionText) {
        mTextView!!.text = null
        if (firebaseVisionText.textBlocks.size == 0) {
            mTextView!!.setText(R.string.error_not_found)
            return
        }
        for (block in firebaseVisionText.textBlocks) {
            mTextView!!.append(block.text)

        }
    }
}